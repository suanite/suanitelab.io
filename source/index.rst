.. blogs documentation master file, created by
   sphinx-quickstart on Tue Nov 28 22:41:21 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to blogs's documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   square-root-algorithm
   redirect-printf
   stm32-makefile-analysis
   eclipse-makefile-openocd
   ISO-week-date
   wxWidgets-configure

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
