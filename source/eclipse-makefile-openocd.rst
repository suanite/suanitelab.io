STM32 makefile 环境搭建
==================================

下载需要的软件
--------------

- `Eclipse IDE for Embedded C/C++ Developers <https://www.eclipse.org/downloads/packages>`_
- `arm-none-eabi-gcc <https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads>`_
- `Windows Build Tools <https://github.com/xpack-dev-tools/windows-build-tools-xpack/releases>`_
- `OpenOCD <https://github.com/openocd-org/openocd/releases>`_

我们选择自带嵌入式插件的 eclipse ，这样就不用再自己安装插件了。选择 arm-none-eabi-gcc 编译器，这是基于 Windows 的针对 arm 32 位 MCU 的 GCC 套件。我们还要 make 工具，这玩意 arm-none-eabi-gcc 是不带的。还要安装 OpenOCD 用于调试 MCU ，如果使用 J-Link 也可以不用安装 OpenOCD 。我们把这几个工具放在同一目录，比如：

.. code:: none

    C:/Applications
        |- arm-none-eabi-gcc
        |- eclipse
        |- OpenOCD
        |- xpack-windows-build-tools

eclipse 的配置
--------------

指定软件路径
++++++++++++

我们打开 eclipse ，新建一个目录，比如 eclipse-embeded 作为工作空间。然后选择 Window->Preferences->MCU->Global Arm Toolchains Paths ，在右侧的 Toolchain folder 选择 arm-none-eabi-gcc 的 bin 目录。

选择 Window->Preferences->MCU->Global Build Tools Path ，在右侧的 Build tools folder 选择 xpack-windows-build-tools 的 bin 目录。

如果我们选择 OpenOCD 调试器，需要选择 Window->Preferences->MCU->Global OpenOCD Path ，在右侧的 Executable 填入 openocd.exe ， Folder 选择 OpenOCD 的 bin 目录。

如果是选择 J-Link 调试器，需要指定 J-Link 的路径，参考上一段 OpenOCD 的设置。

安装芯片包
++++++++++

安装芯片包分为在线安装和离线解压两种方式。

在线安装分三步。第一步点击工具栏 Make the CMSIS Packs perspective visible 图标；第二步点击 Update the CMSIS Packs definitions 图标更新芯片包索引，更新索引会花费大量的时间，甚至有一些芯片包不存在，我们需要手动跳过弹窗；第三步选择要安装的芯片包，右键点击 Install 进行安装。

离线安装。在 `arm CMSIS Packs <https://www.keil.arm.com/packs/>`_ 搜索我们想要的芯片包并下载，我们以 STM32F030C8 为例，下载 STM32F0xx_DFP 。点击 Window->Preferences->C/C++->MCU Packages 查看 CMSIS-Packs 的具体路径。然后把下载下来的芯片包复制到 CMSIS-Packs 的 .cache 目录。然后关闭 eclipse ，并打开 cmd ，跳转到 eclipse.exe 所在的位置。输入命令： ``eclipse.exe -clean`` 然后会看到 Packs 界面出现了芯片包数据，选择芯片包右键安装即可。

编译工程
---------

我们利用 STM32CubeMX 生成一个最简单的 Makefile 工程。芯片选择选择 STM32F030C8 ，勾选 SWD 接口，工程名 makefile-example ，路径选择 eclipse 的工作空间目录 eclipse-embeded 。

生成代码之后在 eclipse 中选择 File->New->Makefile Project with Existing Code ，代码路径选择 Makefile 所在的目录，工具链选择 Arm Cross GCC 。导入 Makefile 工程之后，右键工程名选择 Properties->C/C++ Build->Settings->Devices->STM32F030C8 。

GCC 编译时会出现链接警告 ``elf has a LOAD segment with RWX permissions`` 。建议链接时加上 ``-Wl,--no-warn-rwx-segments`` 屏蔽该警告。

旧版本的 STM32CubeMX 生成的 makefile 工程，在编译时还会有缺少 ``_write`` ， ``_close`` 等系统接口的错误。这是因为 makefile 的链接选项里指定了 ``-lnosys`` ，意思是不使用系统相关的接口，所以需要我们自行定义对应的系统接口。新版本的 STM32CubeMX 生成的代码中多了一个 ``syscall.c`` 文件，里面贴心地定义一些系统接口用于通过编译。我们还能定义 ``__io_putchar`` 函数来重定向实现 ``printf`` 函数。

调试工程
--------

接下来我们配置 OpenOCD 用于调试工程。

我们选择 Run->Debug Configurations...->GDB OpenOCD Debugging ，然后双击打开配置界面。切换到 Debugger 标签页，在 Config options 文本框填入

.. code:: none

    -s ${openocd_path}/../share/openocd/scripts -c "source [find interface/cmsis-dap.cfg];source [find target/stm32f0x.cfg]"

切换到 SVD Path 标签页，文件路径选择

.. code:: none

    ${cmsis_pack_path}/Keil/STM32F0xx_DFP/2.1.1/CMSIS/SVD/STM32F0x0.svd

点击 Debug 按键即可进入调试模式。至此 eclipse 加 openocd 编译调试 stm32 makefile 工程的环境就搭建好了。
