wxWidgets 开发环境配置
=======================

安装 Mingw64
-------------

MinGW-w64 各版本下载页在 `这里 <https://www.mingw-w64.org/downloads/>`_ 。其中 Windows 平台的 Mingw 发行版在 `GitHub <https://github.com/niXman/mingw-builds-binaries/releases>`_ 上，推荐下载 posix-seh-ucrt 版本。添加环境变量的操作这里就不介绍了。


wxWidgets 编译
--------------

当前最新稳定版的 wxWidgets 是 3.2.4 ，我们到 `wxWidgets 官网 <https://www.wxwidgets.org/downloads/>`_ 下载源码包， zip 或者 7z 压缩包任选其一。下载解压压缩包，然后进入到 build/msw 目录。我个人比较喜欢用 git bash ，解压后的文件我放在 C:/Applications/wxWidgets/3.2.4 。因此用 git bash 跳转

.. code:: bash

    cd /c/Applications/wxWidgets/3.2.4/build/msw

清理源代码：

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=release clean

然后编译库：

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=release

从上面我们可以看出基本的编译选项有 3 个：

SHARED 控制 wxWidgets 是构建动态库（ SHARED=1 ） 还是静态库 （ SHARED=0 ）。利用构建的 DLL ，主程序构建时间较快，执行文件更小。对应的缺点是可执行文件加上 wxWidgets DLL 的总大小更大，但是不同的可执行文件可以使用同一个 DLL 。

- wxWidgets 的动态构建会创建导入库（如 libwxbase32u.a ）以及 DLL 文件（如 wxbase32u_gcc_custom.dll ）。在发布程序时必须包含对应的 DLL 。动态构建的库输出目录是 /lib/gcc_dll 。

- wxWidgets 的静态构建只会创建静态库（如 libwxbase32u.a ），发布的时候也无须包含 wxWidgets 的 DLL 。静态构建的库输出目录是 /lib/gcc_lib 。

UNICODE 控制 wxWidgets 以及你的程序是否使用支持 Unicode 的宽字符串。大多数 Windows 2000 或更高系统上的应用程序都应该支持 Unicode 。早期的 Windows 版本不一定有 Unicode 支持。你应该总是使用 wxWidgets 的 _("string") 和 _T("string") 宏来确保硬编码的字符串编译时是正确的类型。

- wxWidgets 的 Unicode （ UNICODE=1 ）构建将会创建带有「 u 」后缀的库，例如「 libwxbase32u.a 」和「 wxbase32u_gcc_custom.dll 」。 wxWidgets 的 Unicode 构建会在 wxWidgets 库的输出目录中创建「 mswu 」或「 mswud 」目录。

- wxWidgets 的 ANSI （ UNICODE=0 ）构建的库没有「 u 」后缀，例如「 libwxbase32.a 」和「 wxbase32_gcc_custom.dll 」。 wxWidgets 的 ANSI 构建会在 wxWidgets 库的输出目录中创建「 msw 」或「 mswd 」目录。

BUILD 控制 wxWidgets 构建调试版本 （ BUILD=debug ）或者是发布版本（ BUILD=release ）。

- debug 构建 wxWidgets 会创建带有「 d 」后缀的库，例如「 libwxbase32ud.a 」和「 wxbase32ud_gcc_custom.dll 」。同时会在 wxWidgets 库的输出目录中创建「 mswd 」或者「 mswud 」目录。

- release 构建 wxWidgets 创建的库没有「 d 」后缀，例如「 libwxbase32u.a 」和「 wxbase32u_gcc_custom.dll 」。同时会在 wxWidgets 库的输出目录中创建「 mswd 」或者「 mswud 」目录。

另外常用的一个选项是 MONOLITHIC 。 MONOLITHIC 控制是构建一个单一的库（ MONOLITHIC=1 ）还是多个组件库（ MONOLITHIC=0 ）。使用单一构建，项目的设置和开发会更加简单，如果你同时使用 DLL 构建的话，你只需要分发一个 DLL 文件。如果使用非单一构建（ multilib ），会构建出多个不同的库同时你可以避免将整个 wxWidgets 的基本代码链接到主程序，就可以去掉不需要的库。当然你也必须确保你选择了正确的组件库（建立工程的时候有个库组件选项）。

- wxWidgets 的单一构建仅会创建一个 wxWidgets 导入库（如 libwxbase32u.a ）以及一个 DLL （如 wxbase32u_gcc_custom.dll ）。

- wxWidgets 的多库（ multilib ）构建会创建多个导入库（ libwxbase32u.a 等）以及多个 DLL 文件。

- 无论何种 wxWidgets 构建，都会创建额外的静态库（如 libwxexpat.a 、 libwxjpeg.a 等）。这些库对于 wxWidgets 的 DLL 构建一般是不需要的，但是当使用静态构建的时候，则是必须的。

编译 wxWidgets 库根据 CPU 的性能需要 10 分钟到半小时不等，我们可以用 -j 参数使用多线程编译，加快编译速度，比如 -j4 使用 4 核心同时编译库。

我一般会编译 4 个版本的库备用，每次编译前最好 clean 清理一下中间文件。

Unicode 编码 debug 类型的动态库

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=debug

Unicode 编码 release 类型的动态库

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=release

Unicode 编码 debug 类型的静态库

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=0 UNICODE=1 BUILD=debug

Unicode 编码 release 类型的动态库

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=0 UNICODE=1 BUILD=release

wxWidgets 源代码的 samples 目录中包含很多示例，是很好的学习对象。刚刚我们编译了 4 个类型的库，编译示例跟编译库的方式差不多。我们以编译最小示例为代表，跳转到 samples/minimal 目录，编译静态链接的 minimal ：

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=0 UNICODE=1 BUILD=debug clean
    mingw32-make -f makefile.gcc SHARED=0 UNICODE=1 BUILD=debug

编译完成后会生成 gcc_mswud 目录，直接点击 minimal.exe 即可运行程序。

编译动态链接的 minimal:

.. code:: bash

    mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=debug clean
    mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=debug

编译完成后会生成 gcc_mswuddll 目录，直接点击 minimal.exe 会提示缺少动态库，我们需要从 lib/gcc_dll 目录中复制对应的动态库到 gcc_mswuddll 目录，才能运行 minimal.exe 。

另外由于编译示例时没有使用 gcc 的 -static 选项静态链接标准库，如果需要分发示例给其他人需要把对应的 Mingw64 动态库一起复制到 minimal.exe 同一目录中。在我电脑上能直接运行 minimal.exe 是因为我已经把 Mingw64 添加到环境变量里。这一点在以后编写自己的程序时也需要注意。

CodeBlocks 环境搭建
--------------------

Code::Blocks 当前稳定版是 20.03 ，我们可以直接从 `Code::Blocks 官网 <https://www.codeblocks.org/>`_ 官网下载稳定版 20.03 ，或者在 `Code::Blocks 论坛 <https://forums.codeblocks.org/index.php/board,20.0.html>`_ 下载 Nightly build 版本。我个人喜欢使用 Nightly 版本。首先我们进入 Nightly builds 论坛分区，点进最新的帖子，点击三个链接分别下载三部分内容，包括

- Code::Blocks 的 wxWidgets 动态库
- Mingw64 动态库
- Code::Blocks 主体程序

下载后把这三部分内容解压缩放到同一目录，比如我放在 C:/Applications/CodeBlocks 中。第一次运行 Code::Blocks 时点击 CbLauncher.exe ，会自动检测到编译器 GNU GCC compiler 。如果没检测到我们也可以进入 Code::Blocks 后在 Settings->Compiler->Global Compiler settings->Toolchain executables 选项卡里手动选择编译器。

然后配置 wxWidgets 环境变量。打开 Settings->Global variables ，新建名为「 wx 」的变量，路径选择 wxWidgets 源代码目录 C:/Applications/wxWidgets/3.2.4 。

接下来新建 wxWidgets demo 工程。创建新工程，在工程管理窗口选择 wxWidgets project ， wxWidgets 版本选择 3.2 。选择项目路径和项目名，我这里取名为 HelloWorld ，个人信息可填可不填。 GUI Builder 选择 wxSmith ，应用类型选择 Frame Based ， wxWidgets 环境变量我们配置过了，直接下一步。编译器用默认的即可。 wxWidgets 库设置和上文编译的配置保持一致，包括是否支持 Unicode ，支持动态库还是静态库。最后一个窗口按实际项目需求选择额外的库。

至此， Code::Blocks 会生成一个空白窗口，可以直接编译运行。关于可能缺失 DLL 的问题参考前文编译 wxWidgets 示例的说明。

参考文档

`Compiling wxWidgets with MinGW <https://wiki.wxwidgets.org/Compiling_wxWidgets_with_MinGW>`_

`wxWidgets 在 Windows 下开发环境配置 <https://www.cnblogs.com/gaowengang/p/5822132.html>`_
